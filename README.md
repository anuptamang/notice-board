# Projects scaffolding tool based on Webpack

## System Environment

- `NodeJS v10+`

## Install project dependencies

- `npm install`

## Run project

- `npm run dev`

## Build project

- `npm run build`
