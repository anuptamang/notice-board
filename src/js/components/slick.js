import jquery from 'jquery'
import '../plugins/slickPlugin'

// slick init
export default function initSlickCarousel() {
  const heroSlider = jQuery('.hero-slider')

  jQuery('.karyapalika-slider').slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    rows: 0,
    arrows: false,
    dots: false,
    infinite: true,
    adaptiveHeight: false,
    fade: false,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: false,
    accessibility: true,
  })

  jQuery('.team-slider').slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    rows: 0,
    arrows: false,
    dots: false,
    infinite: true,
    adaptiveHeight: false,
    fade: false,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: false,
    accessibility: true,
  })

  jQuery('.wada-slider').slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    rows: 0,
    arrows: false,
    dots: false,
    infinite: true,
    adaptiveHeight: false,
    fade: false,
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false,
    accessibility: true,
  })

  jQuery(heroSlider).slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    rows: 0,
    arrows: false,
    dots: false,
    infinite: false,
    adaptiveHeight: false,
    fade: true,
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false,
    accessibility: true,
  })

  const videoSection = document.querySelectorAll('[data-video-intro]')

  videoSection.forEach((section) => videoAction(section))

  function videoAction(section) {
    const videoBlock = section.querySelector('[data-video]')
    videoBlock[0]?.pause()

    videoBlock.addEventListener('ended', (event) => {
      jQuery(heroSlider).slick('slickPlay')
    })
  }

  jQuery(heroSlider).on(
    'afterChange',
    function (event, slick, currentSlide, nextSlide) {
      // console.log(slick[currentSlide])
      if (jQuery('.slick-current').hasClass('video')) {
        jQuery(heroSlider).slick('slickPause')
        jQuery('.slick-current').find('[data-video]')?.[0]?.play()
      } else {
        // jQuery('#data-video')[0]?.pause()
        // jQuery(heroSlider).slick('slickPlay')
      }

      if (jQuery('.slick-current').hasClass('news')) {
        jQuery(heroSlider).slick('slickPause')
        // jQuery('.news-slider').slick('slickPlay')
        $('a[data-fancybox="gallery"]').first().trigger('click')
      } else {
        // $.fancybox.close()
        // jQuery(heroSlider).slick('slickPlay')
      }
    }
  )
}
