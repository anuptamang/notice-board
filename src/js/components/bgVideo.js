import '../plugins/bgVideoPlugin'

// background video init
export default function initBackgroundVideo() {
  jQuery('.bg-video').backgroundVideo({
    activeClass: 'video-active',
  })
}
