import '../plugins/fancyboxPlugin'

// lightbox init
export default function initFancybox() {
  jQuery('a.lightbox, [data-fancybox]').fancybox({
    parentEl: 'body',
    margin: [50, 0],
    transitionEffect: 'slide',
    loop: false,
    keyboard: true,
    infobar: true,

    slideShow: {
      autoStart: true,
      speed: 30000,
      loop: false,
    },

    afterShow: function (instance, current) {
      if (current.index === instance.group.length - 1) {
        jQuery('.hero-slider').slick('slickPlay')
        // console.log('afterload... 1')
        let timer1, timer2
        clearTimeout(timer1)
        timer1 = setTimeout(() => {
          $.fancybox.destroy()
          // console.log('afterload... 2')

          if (jQuery('.slick-current.news').next().length === 0) {
            // jQuery('.hero-slider').slick('slickPlay')

            clearTimeout(timer2)
            timer2 = setTimeout(() => {
              // console.log('afterload... 3')
              jQuery('.hero-slider').slick('slickGoTo', +1)
              jQuery('.hero-slider').slick('slickPlay')
            }, 500)
            jQuery('.hero-slider').slick('slickPlay')
            // console.log('afterload... 4')
            // return false
          }
        }, 3000)
      }
    },
  })
}
