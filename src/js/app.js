import initMarquee from './components/marquee'
import initSlick from './components/slick'
import initBgVideo from './components/bgVideo'
import initFancybox from './components/fancybox'

import ready from './utils/global'

ready(() => {
  initSlick()
  initMarquee()
  initFancybox()
  // initBgVideo()
})
